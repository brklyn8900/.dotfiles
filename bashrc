#PS1="\w \n \d \t:\u:\w>$ "

export PS1=" \n \[\033[33;1m\]\w\[\033[m\] \n \[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h: "
export CLICOLOR=1
export LSCOLORS=ExFxBxDxCxegedabagacad

if [ -f ~/.bash_aliases ]; then . ~/.bash_aliases; fi

##
# Your previous /Users/ron/.bash_profile file was backed up as /Users/ron/.bash_profile.macports-saved_2015-11-22_at_07:36:07
##


# MacPorts Installer addition on 2015-11-22_at_07:36:07: adding an appropriate PATH variable for use with MacPorts.
export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
export PATH="~/ruby/bin:$PATH"


export CLICOLOR=1
export LSCOLORS=DxGxBxDxCxEgEdxbxgxcxd
export PS1="\e[41m $Time12h $PathShort \e[0m \$(git_status) \n \$ "
GIT_PS1_SHOWDIRTYSTATE=true

source ~/.git-prompt.sh

[ -s "/Users/ron/.scm_breeze/scm_breeze.sh" ] && source "/Users/ron/.scm_breeze/scm_breeze.sh"


source ~/.profile

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"
