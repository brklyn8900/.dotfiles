if [ -f ~/.bashrc ]; then
   source ~/.bashrc
fi
export PATH=$PATH:~/.nexustools

# added by Anaconda3 4.3.0 installer
export PATH="/Users/ron/anaconda/bin:$PATH"

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
