
alias ls='ls -GFhla'
alias cd..="cd .."
alias l="ls -al"
alias lp="ls -p"
alias h=history
alias myip="node ~/all-files/Dropbox/ron/devstuff/getip/get-ip.js"
alias a="atom ~/.bash_aliases"
alias ds="cd ~/all-files/devstuff"
alias r="rails s"
alias rk="rake server"
alias /="clear"
alias at="atom ."
alias soundreset="sudo killall coreaudiod"
alias s="browser-sync start -s -w"
alias ka="sudo killall coreaudiod"
alias nx="/Users/ron/all-files/devstuff/shellscripts/nuxt.sh"
alias ya="/Users/ron/all-files/devstuff/shellscripts/ytdl-a.sh"
alias yv="/Users/ron/all-files/devstuff/shellscripts/ytdl-v.sh"

# Currents
#==========
alias cc="cd ~/all-files/devstuff/projects/rails/cornercafe"
alias zc="cd ~/all-files/devstuff/projects/vue/zap-coupons"
alias dr="cd ~/all-files/devstuff/projects/surges/deborah"


# Heroku commands
#==========
alias hk="heroku"
alias gphm="git push heroku master"
alias hrr="heroku run rake db:migrate"
alias hpsw="heroku ps:scale web=1"
alias hps="heroku ps"
alias ho="heroku open"
dep() { [[ -z $1 ]] && return -1; setenv $1; git push "$1" `currbranch`:master; graytab; notify `currbranch` $1; } # Deploy
orangetab() { graytab; echo -ne '\033]6;1;bg;red;brightness;255\a'; echo -ne '\033]6;1;bg;green;brightness;165\a'; echo -ne '\033]6;1;bg;blue;brightness;0\a'; } # Make tab orange
graytab()   { echo -ne "\033]6;1;bg;*;default\a"; } # Make tab black
setenv()    { [ "$1" = "production" ] && redtab; [ "$1" = "staging" ] && orangetab; }
notify()    { osascript -e 'display notification "Branch: '$1'" with title "Deployed to '$2'!"'; }
currbranch() { git symbolic-ref --short HEAD; }

# Git
#==========
alias gpm="git push origin master"

# Docker
#==========
alias dcls="docker container ls"
alias dcla="docker container ls -a"
alias dnls="docker network ls"

# Python
#==========
alias ve="virtualenv venv"
alias ve3="virtualenv -p /usr/local/bin/python3 venv"
alias va="source venv/bin/activate"
alias ved="deactivate"
alias py="python3"

alias rel="source ~/.zshrc"
alias profile="atom ~/.bash_profile"

# Colors
# =========
Color_Off="\[\033[0m\]"       # Text Reset
Yellow="\[\033[0;33m\]"       # Yellow
IBlack="\[\033[0;90m\]"       # Black

# Various variables you might want for your PS1 prompt instead
Time12h="\T"
Time12a="\@"
PathShort="\w"
PathFull="\W"
NewLine="\n"
Jobs="\j"

git_prompt_info() {
  ref=$(git symbolic-ref HEAD 2>/dev/null) || return
  echo "${ref#refs/heads/}"
}
unpushed()  { git cherry -v @{upstream} 2>/dev/null | wc -l | tr -d ' '; }
need_push() {
  return 0
  unpushed_count=$(unpushed)
  if ! [[ $(unpushed) == "0" ]]; then
    echo -en "\033[0;33m (+$unpushed_count)\033[0m"
  fi
}
git_status() {
  if git branch &> /dev/null; then
    if  git status | grep "nothing to commit" &> /dev/null ; then
      # @4 - Clean repository - nothing to commit
      echo -en "$(tput setaf 2)$(__git_ps1 " (%s)")"$(tput sgr0);
    else
      # @5 - Changes to working tree
      echo -en $(tput setaf 1)$(__git_ps1 " {%s}")$(tput sgr0);
    fi
    need_push
  fi
}
# Shortcut for github
github() {
  curr_folder=$(current_folder)
  if [[ $curr_folder == "fiverr_v2" ]]; then
    curr_folder='5rr_v2';
  fi
  open "https://github.com/fiverr/$curr_folder/tree/$(git_prompt_info)";
}

gitpr() {
  if [ -z "$1" ]
  then
    echo "No commit message was supplied"
    return
  fi
  curr_branch=`git_prompt_info`
  git checkout master
  git merge $curr_branch --squash
  git checkout -b "$curr_branch"_pr
  git commit -nm "$1"
  git push --set-upstream origin "$curr_branch"_pr
  `github`
}
